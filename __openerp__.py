# -*- coding: utf-8 -*-
 

{
    'name': 'account_report_inherit',
    'version': '1.0',
    'category': 'Base',
    'description': """
    为 报表 --> 会计 --> 分录分析 数据透视表添加期初余额
    """,
    'author': 'lmch',
    'website': 'YJ_ERP',
    'depends': ['base','account'],
    'data': [
    ],
    "data": [
        "account_report_inherit.xml", 
    ],     
    'installable': True,
    'auto_install': False,
}

